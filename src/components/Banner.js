import React from 'react';

const Banner = () => {
    return (
        <div className='hero'>
            <aside className='hero__content'>
                <p className='hero__content--subtile'>No fees.</p>
                <p className='hero__content--subtile'>NO minimum deposit</p>
                <p className='hero__content--subtile'>High interest rates</p>
                <p className='hero__content--text'> Open a savings account with <br /> Argent Bank today</p>
            </aside>
        </div>
    );
};

export default Banner;