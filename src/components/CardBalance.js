import PropTypes from 'prop-types';
import React from 'react';
import { NavLink } from 'react-router-dom';

const CardBalance = ({ title, amount, description }) => {


    return (
        <section className='user__account'>
            <div className='user__account--content'>
                <h3 className='account-title'>{title}</h3>
                <p className='account-amount'>{amount}</p>
                <p className='account-description'>{description}</p>
            </div>
            <div className='user__account--transaction'>
                <NavLink to='/profile/transaction'>
                    <button className='transaction-button'>
                        View transactions
                    </button>
                </NavLink>
            </div>

        </section>
    );
};

export default CardBalance;

CardBalance.propTypes = {
    title: PropTypes.string.isRequired,
    amount: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
}