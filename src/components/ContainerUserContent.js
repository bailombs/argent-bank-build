import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { apiEdit, PROFILE_ROUTE } from '../api/routes';
import { firstName, lastName } from '../features/slicer';
import CardBalance from './CardBalance';

const ContainerUserContent = () => {
    const [edit, setEdit] = useState(false)
    const [inputFirstName, setInputFirstName] = useState('')
    const [inputLastName, setInputLastName] = useState('')

    const token = useSelector((state) => state?.connect?.token)
    const userFullName = useSelector((state) => state.connect)

    const dispatch = useDispatch()

    const viewTransactions = [
        {
            title: 'Argent Bank Checking (x8349)',
            amout: '$2,082.79',
            description: 'Available Balance'
        },
        {
            title: 'Argent Bank Savings (x6712)',
            amout: '$10,928.42',
            description: 'Available Balance'
        },
        {
            title: 'Argent Bank Card(x8349)',
            amout: '$184.30',
            description: 'Current Balance'
        }
    ]





    // function handle to edit
    const handleEdit = () => {
        setEdit(true)
    }

    // function handle to cancel
    const handleCancel = () => {
        setEdit(false)
    }

    // function handle to save modification
    const handleSave = async () => {

        try {
            if (inputFirstName.trim().length !== 0 && inputLastName.trim().length !== 0) {
                setEdit(false)
                const res = await apiEdit(PROFILE_ROUTE, inputFirstName, inputLastName, token);
                dispatch(firstName(res.data.body?.firstName))
                dispatch(lastName(res.data.body?.lastName))
            } else {
                setEdit(true)
            }

        } catch (error) {
            console.log(error);
        }
    }

    return (
        <main className='user'>
            <div className='user__welcom'>
                <h1 className='user__welcom--name'> Welcome Back </h1>
                {edit ? (
                    <section className='user__welcom--edit'>
                        <input type="text"
                            placeholder={userFullName?.firstName}
                            onChange={(e) => setInputFirstName(e.target.value)}
                        />
                        <input type="text"
                            placeholder={userFullName?.lastName}
                            onChange={(e) => setInputLastName(e.target.value)}
                        />
                        <button className='btn-left' onClick={() => { handleSave() }}>Save</button>
                        <button className='btn-right' onClick={() => handleCancel()}>Cancel</button>
                    </section>
                ) : (
                    <React.Fragment>
                        <p className='user__welcom--full-name'>{`${userFullName?.firstName}  ${userFullName?.lastName} !`}</p>
                        <button className='user__welcom--button-edit' onClick={() => { handleEdit() }} >Edit Name</button>
                    </React.Fragment>
                )}
            </div>
            {
                viewTransactions.map((viewTransaction, index) =>
                    <CardBalance
                        key={index}
                        title={viewTransaction.title}
                        amount={viewTransaction.amout}
                        description={viewTransaction.description}
                    />
                )
            }
        </main>
    );
};

export default ContainerUserContent;