import PropTypes from 'prop-types';
import React from 'react';

const Feature = ({ icon, title, text }) => {
    return (
        <div className='features__item'>
            <img src={icon} alt='icon' className='features__item--icon' />
            <h3 className='features__item--title'>{title}</h3>
            <p>{text}</p>
        </div>
    );
};

export default Feature;

Feature.propTypes = {
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
}

