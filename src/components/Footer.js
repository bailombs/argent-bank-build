import React from 'react';

const Footer = () => {
    return (
        <footer className='footer'>
            <p>Copyright 2022 Argent BanK</p>
        </footer>
    );
};

export default Footer;