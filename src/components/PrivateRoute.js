import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';

const PrivateRoute = () => {

    const token = sessionStorage.getItem('token')

    //custom user hook
    const useAuth = () => {

        const user = { loggedIn: token }
        return user && user.loggedIn
    }

    const isAuth = useAuth()

    return (
        isAuth ? <Outlet /> : <Navigate to='/sign-in' />
    )
};

export default PrivateRoute;