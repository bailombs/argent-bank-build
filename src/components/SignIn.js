import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FaUserCircle } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { LOGIN_ROUTE } from '../api/routes';
import { getToken, postData } from '../features/slicer';



const SignIn = () => {

    const [errorMessage, setErrorMessage] = useState('')
    const [registerEmail, setRegisterEmail] = useState('')
    const [registerPassword, setRegisterPassword] = useState('')

    let formatMailRegex = /^[a-z0-9.-_]+[@]{1}[a-z.-_]+[.]{1}[a-z]{2,8}$/

    const naviagte = useNavigate()

    useEffect(() => {
        setErrorMessage(false)
    }, [registerEmail, registerPassword])



    const dispatch = useDispatch()
    const loginSucessDispatch = useDispatch()

    const handleSubmit = async (e) => {
        e.preventDefault()

        if (formatMailRegex.test(registerEmail) === false) {
            setErrorMessage('veuillez entrer une adresse mail correcte')
            return
        } else if (registerPassword === '') {
            setErrorMessage('veuillez entrer votre mot de passe')
            return
        }

        try {
            const res = await axios.post(LOGIN_ROUTE, {
                "email": registerEmail,
                "password": registerPassword,
            }
            )
            sessionStorage.setItem('token', res.data.body?.token)

            // const token = sessionStorage.getItem('token')
            loginSucessDispatch(getToken(res.data.body?.token))
            dispatch(postData(registerEmail))

            /**
             * check if token exist
             */
            naviagte('/profile');

            setRegisterEmail('')
            setRegisterPassword('')
        } catch (error) {
            if (!error?.response) {
                setErrorMessage('Pas de reponse de server')
            }
            else if (error?.response?.status === 400) {
                setErrorMessage('Votre mail ou mot de passe ne correpondent pas')
            }
            else if (error?.response?.status === 401) {
                setErrorMessage('vous n\'avez pas d\'autorisation')
            } else if (!error?.response) {
                setErrorMessage('Pas de reponse du server')
            } else {
                setErrorMessage('la connexion au server a echoué')
            }
        }
        console.log(registerEmail, '===', registerPassword);
    }

    return (
        <main className='sign-in'>
            <section className='sign-in__content'>
                <FaUserCircle />
                <h1 className='sign-in__title'>Sign In</h1>
                <form className='form' onSubmit={(e) => handleSubmit(e)}>
                    <div className='form__inptwrapper'>
                        <label htmlFor='email'>Username</label>
                        <input type="text" id='email' onChange={(e) => { setRegisterEmail(e.target.value) }} />
                    </div>
                    <div className='form__inptwrapper'>
                        <label htmlFor='password'>Password</label>
                        <input type="password" id='password' onChange={(e) => { setRegisterPassword(e.target.value) }} />
                    </div>
                    {
                        errorMessage ?
                            <p className='form__error'>{errorMessage}</p> : null
                    }
                    <div className='form__remember'>
                        <input type="checkbox" id="remember-me" />
                        <label htmlFor='remember-me'>Remember me</label>
                    </div>
                    <input type="submit" value="Sign In" className='form__sign' />
                </form>

            </section>
        </main>
    );
};

export default SignIn;