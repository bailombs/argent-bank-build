import React from 'react';
import Banner from '../components/Banner';
import ContainerFeatures from '../components/ContainerFeatures';
import Footer from '../components/Footer';
import Header from '../components/Header';

const Home = () => {
    return (
        <React.Fragment>
            <Header />
            <Banner />
            <ContainerFeatures />
            <Footer />
        </React.Fragment>
    );
};

export default Home;