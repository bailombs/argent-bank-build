import React from 'react';
import Footer from '../components/Footer';
import Header from '../components/Header';
import SignIn from '../components/SignIn';

const Login = () => {
    return (
        <React.Fragment>
            <Header />
            <SignIn />
            <Footer />
        </React.Fragment>
    );
};

export default Login;